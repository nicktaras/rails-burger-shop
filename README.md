# README #

Burger shop test with Ruby on the Rails.

Using the Repo:

rails s (runs server)
ctrl c to stop server
rails g controller home index (Create files needed for index)
rails g scaffold Burger name price:decimal description:text (make a Burger object with parameters)
rake db:migrate (fix the error)

Useful links for further learning:

http://learnrubythehardway.org/book/
https://github.com/harthur/brain
https://developer.apple.com/library/ios/referencelibrary/GettingStarted/RoadMapiOS/SecondTutorial.html#//apple_ref/doc/uid/TP40011343-CH8-SW1